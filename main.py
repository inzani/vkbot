from vk_api import VkApi
from vk_api.longpoll import VkLongPoll, VkEventType
from vk_api.utils import get_random_id
from datetime import datetime
import pyowm
import random
import Module.moduleFunctionBot as moduleFunctionBot

weather_list = ["?погода", "погода?", "?Погода", "Погода?"]
bot_list = ["?bot", "bot?", "бот?", "?бот", "?Бот", "Бот?"]
fact_list = ["?факт", "факт?", "?Факт", "Факт?"]
prank_list = ["?прикол", "?приколы", "прикол?", "приколы?", "Прикол?"]
token = ""
vk_session = VkApi(token=token)
dt_now = datetime.now()

try:
    vk_session._auth_token()
except token.AuthError as error_msg:
    moduleFunctionBot.logs("\n"+error_msg+" Ошибка авторизации токена в Vk ")
longpoll = VkLongPoll(vk_session)
vk = vk_session.get_api()


def write_msg():
    for event in longpoll.listen():
        if event.type == VkEventType.MESSAGE_NEW:
            vk.messages.send(
                random_id=get_random_id(),
                peer_id=event.obj['peer_id'],
                message='Test message',
            )


for event in longpoll.listen():
    if event.type == VkEventType.MESSAGE_NEW and event.to_me and event.text:
        if event.from_user:
            moduleFunctionBot.logs("\n"+str(dt_now)+"id user - "+str(event.user_id)+" message "+event.text +
                                     "  -  {personal_message} ")
            if event.text in bot_list:
                vk.messages.send(
                    user_id=event.user_id,
                    message="Добрый день, чтобы воспользоваться ботом Введите следующие команды" "\n"
                            "?факт""\n""?прикол,""\n" 
                            " ?погода + город", random_id=random.randint(1, 10000))

            elif event.text[:7] in weather_list:
                city = event.text[8:]
                owm = pyowm.OWM("430a9e59a227ceadb9af7eca987a35d9")
                mgr = owm.weather_manager()
                observation = mgr.weather_at_place(city + ",ru")
                w = observation.weather
                temperature = w.temperature('celsius')["temp"]
                wind = w.wind()["speed"]
                vk.messages.send(
                    user_id=event.user_id,
                    message="Температура сейчас "+str(temperature)+"С"+"\n""Скорость ветра: " +
                            str(wind)+"м/с", random_id=random.randint(1, 10000))

            elif event.text in fact_list:
                vk.messages.send(
                    user_id=event.user_id,
                    message=moduleFunctionBot.fact(), random_id=random.randint(1, 10000))

            elif event.text in prank_list:
                vk.messages.send(
                    user_id=event.user_id,
                    message=moduleFunctionBot.prank(), random_id=random.randint(1, 10000))

        elif event.from_chat:
            moduleFunctionBot.logs("\n"+"id user - "+str(event.user_id)+" message "+event.text+"  -  {discussion} ")

            if event.text in bot_list:
                vk.messages.send(
                    chat_id=event.chat_id,
                    message="Добрый день, чтобы воспользоваться ботом Введите следующие команды" "\n"
                            "?факт""\n""?прикол""\n"
                            " ?погода + город" "\n" "?расследование", random_id=random.randint(100000000, 900000000))

            elif event.text[:7] in weather_list:
                city = event.text[8:]
                owm = pyowm.OWM("430a9e59a227ceadb9af7eca987a35d9")
                mgr = owm.weather_manager()
                observation = mgr.weather_at_place(city + ",ru")
                w = observation.weather
                temperature = w.temperature('celsius')["temp"]
                wind = w.wind()["speed"]
                vk.messages.send(
                    chat_id=event.chat_id,
                    message="Температура сейчас " + str(temperature) + "С" + "\n""Скорость ветра: " +
                            str(wind) + "м/с", random_id=random.randint(100000000, 900000000))

            elif event.text == "?Расследование":
                vk.messages.send(
                    chat_id=event.chat_id,
                    message="Расследование закончено", random_id=random.randint(100000000, 900000000))

            elif event.text in fact_list:
                vk.messages.send(
                    chat_id=event.chat_id,
                    message=moduleFunctionBot.fact(), random_id=random.randint(100000000, 900000000))

            elif event.text in prank_list:
                vk.messages.send(
                    chat_id=event.chat_id,
                    message=moduleFunctionBot.prank(), random_id=random.randint(100000000, 900000000))
            else:
                vk.messages.send(
                    chat_id=event.chat_id,
                    message="Не понял ваш вопрос", random_id=random.randint(1, 10000))