from random import randint
from bs4 import BeautifulSoup
import requests

headers = {
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 '
                      '(KHTML, like Gecko) Chrome/94.0.4606.85 YaBrowser/21.11.0.1996 Yowser/2.5 Safari/537.36'
      }
url_fact = "https://fishki.net/tag/fakty/"
url_prank = "https://fishki.net/3968752-smeshnye-zametki-byvalyh-alkogolikov.html"
weather_owm = ""


def prank():
    response = requests.get(url=url_prank, headers=headers)
    soup = BeautifulSoup(response.text, 'lxml')
    find_prank = soup.find_all('div', class_='picture-relative')
    find_prank_list = []
    for f_e in find_prank:
        find_prank = f_e.find('img')["src"]
        try:
            find_prank = find_prank
            find_prank_list.append(find_prank)
        except:
            None
    len_find_prank_list = len(find_prank_list)
    a = randint(1, len_find_prank_list - 1)
    return find_prank_list[a]


def fact():
    response = requests.get(url=url_fact, headers=headers)
    soup = BeautifulSoup(response.text, 'lxml')
    find_evidence = soup.find_all('div', class_='content__text')
    find_evidence_list = []
    for f_e in find_evidence:
        find_evidence = f_e.find('p', class_='expanded-anounce')
        try:
            find_evidence = find_evidence.text.strip()
            find_evidence.replace(" ", "")
            find_evidence_list.append(find_evidence)
            find_evidence_list.remove('')
        except:
            None
    len_find_evidence_list = len(find_evidence_list)
    a = randint(1, len_find_evidence_list - 1)
    return find_evidence_list[a]


def logs(information):
    with open("log.txt", "a") as log:
        log.write(information)
